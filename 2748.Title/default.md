<article class="ltx_document ltx_authors_1line" prefix="dcterms: http://purl.org/dc/terms/">
<h1 class="ltx_title ltx_title_document">Title</h1>
<div class="ltx_authors">
<span class="ltx_creator ltx_role_author">
<span class="ltx_personname">nate
</span><span class="ltx_author_notes"><span>
<span class="ltx_contact ltx_role_affiliation">Zhengzhou University
</span></span></span></span></div>
<div class="ltx_date ltx_role_creation">December 13, 2017</div>

<figure id="S0.F1" class="ltx_figure"><img src="/title/figures/a-b/a-b.png" id="S0.F1.g1" class="ltx_graphics ltx_centering" alt=""></img>
<figcaption class="ltx_caption ltx_centering" property="dcterms:alternative" content="325576"><span class="ltx_tag ltx_tag_figure">Figure 1: </span>This is a caption
</figcaption>
</figure>
<div class="ltx_pagination ltx_role_newpage"></div>
</article>
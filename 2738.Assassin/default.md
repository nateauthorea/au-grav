<article class="ltx_document ltx_authors_1line" prefix="dcterms: http://purl.org/dc/terms/">
<h1 class="ltx_title ltx_title_document">Assassin</h1>
<div class="ltx_authors">
<span class="ltx_creator ltx_role_author">
<span class="ltx_personname">nate
</span><span class="ltx_author_notes"><span>
<span class="ltx_contact ltx_role_affiliation">Zhengzhou University
</span></span></span></span></div>
<div class="ltx_date ltx_role_creation">December 6, 2017</div>

<div id="p1" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<figure id="S0.F1" class="ltx_figure"><img src="/assassin/figures/single_green_pixel/single_green_pixel.png" id="S0.F1.g1" class="ltx_graphics ltx_centering" alt=""></img>
<figcaption class="ltx_caption ltx_centering" property="dcterms:alternative" content="124147"><span class="ltx_tag ltx_tag_figure">Figure 1: </span>This is a caption
</figcaption>
</figure>
<div id="p2" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at the Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div id="p3" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<div id="p4" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at the Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div id="p5" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<div id="p6" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at the Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div id="p7" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<figure id="S0.F2" class="ltx_figure"><img src="/assassin/figures/red dot/red dot.png" id="S0.F2.g1" class="ltx_graphics ltx_centering" alt=""></img>
<figcaption class="ltx_caption ltx_centering" property="dcterms:alternative" content="350179"><span class="ltx_tag ltx_tag_figure">Figure 2: </span>This is a caption
</figcaption>
</figure>
<div id="p8" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at the Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div id="p9" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<div id="p10" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at the Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div id="p11" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<div id="p12" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at thse Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div id="p13" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the humane society she was a
mangy, pitiful animal. It cost a lot to adopt her: forty dollars. And
then I had to buy litter, a litterbox, food, and dishes for her to eat
out of. Two days after she came home with me she got taken to the pound
by the animal warden. There’s a leash law for cats in Fort Collins. If
they’re not in your yard they have to be on a leash. Anyway, my cat is
my best friend. I’m glad I got her. She sleeps under the covers with me
when it’s cold. Sometimes she meows a lot in the middle of the night and
wakes me up, though. (unfocused)</p>
</div>
<div id="p14" class="ltx_para">
<p class="ltx_p">When I first brought my cat home from the Humane Society she was a
mangy, pitiful animal. She was so thin that you could count her
vertebrae just by looking at her. Apparently she was declawed by her
previous owners, then abandoned or lost. Since she couldn’t hunt, she
nearly starved. Not only that, but she had an abscess on one hip. The
vets at the Humane Society had drained it, but it was still scabby and
without fur. She had a terrible cold, too. She was sneezing and
sniffling and her meow was just a hoarse squeak. And she’d lost half her
tail somewhere. Instead of tapering gracefully, it had a bony knob at
the end. (focused)</p>
</div>
<div class="ltx_pagination ltx_role_newpage"></div>
</article>